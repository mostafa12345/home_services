-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2017 at 12:41 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `home_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblclient`
--

CREATE TABLE `tblclient` (
  `fldid` int(11) NOT NULL,
  `fldname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldfamily` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldaddress` varchar(700) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldmobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldphone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldpassword` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tblclient`
--

INSERT INTO `tblclient` (`fldid`, `fldname`, `fldfamily`, `fldaddress`, `fldmobile`, `fldphone`, `fldpassword`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'test', '24234', '234234', ' gdfsg v2q323v', NULL, NULL),
(2, 'test2', 'test2', 'test2', '234235', '234235', NULL, NULL, NULL),
(3, 'test', 'test', 'test', '24234', '234234', ' gdfsg v2q323v', NULL, NULL),
(4, 'test2', 'test2', 'test2', '234235', '234235', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblctg_services`
--

CREATE TABLE `tblctg_services` (
  `fldid` int(11) NOT NULL,
  `fldtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flddesc` text COLLATE utf8_unicode_ci,
  `fldfile` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldcreated_at` datetime DEFAULT NULL,
  `fldupdated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbldocuments`
--

CREATE TABLE `tbldocuments` (
  `fldid` int(11) NOT NULL,
  `fldtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldexpired` datetime DEFAULT NULL,
  `flddesc` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldfile` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldcreated_at` datetime DEFAULT NULL,
  `fldupdated_at` datetime DEFAULT NULL,
  `tbluser_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblobligation`
--

CREATE TABLE `tblobligation` (
  `fldid` int(11) NOT NULL,
  `fldtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flddesc` varchar(3000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldcreated_at` datetime DEFAULT NULL,
  `fldupdated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblobligation_services`
--

CREATE TABLE `tblobligation_services` (
  `fldid` int(11) NOT NULL,
  `fldtblservices_id` int(11) DEFAULT NULL,
  `fldtbloblication_id` int(11) DEFAULT NULL,
  `fldcreated_at` datetime DEFAULT NULL,
  `fldupdated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblorder`
--

CREATE TABLE `tblorder` (
  `fldid` int(11) NOT NULL,
  `fldtblservices_id` int(11) DEFAULT NULL,
  `fldtbluser_id` int(11) DEFAULT NULL,
  `fldarea` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flddate` date DEFAULT NULL,
  `fldtime` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldip` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldcreated_at` datetime DEFAULT NULL,
  `fldupdated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblservices`
--

CREATE TABLE `tblservices` (
  `fldid` int(11) NOT NULL,
  `fldtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flddesc` text COLLATE utf8_unicode_ci,
  `fldfile` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldtblctg_id` int(11) DEFAULT NULL,
  `fldcreated_at` datetime DEFAULT NULL,
  `fldupdated_at` datetime DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `fldid` int(11) NOT NULL,
  `fldname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldfamily` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldaddress` varchar(700) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldnational_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldShShenas` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldphone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldmobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldpassword` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldemail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fldcreated_at` datetime DEFAULT NULL,
  `fldupdated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblclient`
--
ALTER TABLE `tblclient`
  ADD PRIMARY KEY (`fldid`);

--
-- Indexes for table `tblctg_services`
--
ALTER TABLE `tblctg_services`
  ADD PRIMARY KEY (`fldid`);

--
-- Indexes for table `tbldocuments`
--
ALTER TABLE `tbldocuments`
  ADD PRIMARY KEY (`fldid`),
  ADD KEY `tbluser_id` (`tbluser_id`);

--
-- Indexes for table `tblobligation`
--
ALTER TABLE `tblobligation`
  ADD PRIMARY KEY (`fldid`);

--
-- Indexes for table `tblobligation_services`
--
ALTER TABLE `tblobligation_services`
  ADD PRIMARY KEY (`fldid`),
  ADD KEY `fldtblservices_id` (`fldtblservices_id`),
  ADD KEY `fldtbloblication_id` (`fldtbloblication_id`);

--
-- Indexes for table `tblorder`
--
ALTER TABLE `tblorder`
  ADD PRIMARY KEY (`fldid`),
  ADD KEY `fldtblservices_id` (`fldtblservices_id`),
  ADD KEY `fldtbluser_id` (`fldtbluser_id`);

--
-- Indexes for table `tblservices`
--
ALTER TABLE `tblservices`
  ADD PRIMARY KEY (`fldid`),
  ADD KEY `fldtblctg_id` (`fldtblctg_id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`fldid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblclient`
--
ALTER TABLE `tblclient`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblctg_services`
--
ALTER TABLE `tblctg_services`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbldocuments`
--
ALTER TABLE `tbldocuments`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblobligation`
--
ALTER TABLE `tblobligation`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblobligation_services`
--
ALTER TABLE `tblobligation_services`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblorder`
--
ALTER TABLE `tblorder`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblservices`
--
ALTER TABLE `tblservices`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `fldid` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbldocuments`
--
ALTER TABLE `tbldocuments`
  ADD CONSTRAINT `tbldocuments_ibfk_1` FOREIGN KEY (`tbluser_id`) REFERENCES `tbluser` (`fldid`);

--
-- Constraints for table `tblobligation_services`
--
ALTER TABLE `tblobligation_services`
  ADD CONSTRAINT `tblobligation_services_ibfk_1` FOREIGN KEY (`fldtblservices_id`) REFERENCES `tblservices` (`fldid`),
  ADD CONSTRAINT `tblobligation_services_ibfk_2` FOREIGN KEY (`fldtbloblication_id`) REFERENCES `tblobligation` (`fldid`);

--
-- Constraints for table `tblorder`
--
ALTER TABLE `tblorder`
  ADD CONSTRAINT `tblorder_ibfk_1` FOREIGN KEY (`fldtblservices_id`) REFERENCES `tblservices` (`fldid`),
  ADD CONSTRAINT `tblorder_ibfk_2` FOREIGN KEY (`fldtbluser_id`) REFERENCES `tbluser` (`fldid`);

--
-- Constraints for table `tblservices`
--
ALTER TABLE `tblservices`
  ADD CONSTRAINT `tblservices_ibfk_1` FOREIGN KEY (`fldtblctg_id`) REFERENCES `tblctg_services` (`fldid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
