package org.services.api_facade;

import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import org.homeServices.model.Services;

@Stateless
@Path("services")
public class ServicesFacadeREST extends AbstractFacade<Services> {

    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public ServicesFacadeREST() {
        super(Services.class);
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Services find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Services> findAll() {
        return super.findAll();
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

//    @GET
//    @Path(value = "{from}/{to}")
//    @Produces(value = MediaType.APPLICATION_JSON)
//    @Asynchronous
//    public void findRange(@Suspended final AsyncResponse asyncResponse, @PathParam(value = "from") final Integer from, @PathParam(value = "to") final Integer to, @QueryParam(value = "search") final String search) {
//        asyncResponse.resume(doFindRange(from, to, search));
//    }

    @GET
    @Path(value = "/{id}/{from}/{to}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Services> findRangeByCtg(@PathParam(value = "id") final Integer id, @PathParam(value = "from") final Integer from, @PathParam(value = "to") final Integer to, @QueryParam(value = "search") final String search) {
        return this.findRangeByCtg(id, new int[]{from, to}, search);
    }
    
    @GET
    @Path(value = "{from}/{to}")
    @Produces(value = MediaType.APPLICATION_JSON)
    public List<Services> doFindRange(@PathParam("from") Integer from, @PathParam("to") Integer to, @QueryParam("search") String search) {
        if (search == null) {
            return super.findRange(new int[]{from, to});
        } else {
            return this.findRange(new int[]{from, to}, search);
        }
    }

    private List<Services> doFindRangeByCtg(@PathParam("id") Integer id, @PathParam("from") Integer from, @PathParam("to") Integer to, @QueryParam("search") String search) {
        return this.findRangeByCtg(id, new int[]{from, to}, search);
    }

    public List<Services> findRange(int[] range, String search) {
        javax.persistence.Query q
                = getEntityManager().createQuery("select s from Services s where s.meta like :meta");
        q.setParameter("meta", "%" + search + "%");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    public List<Services> findRangeByCtg(int id, int[] range, String search) {
        javax.persistence.Query q;
        System.out.println("search is : "+search);
        if(search != null && !search.isEmpty()){
            q =  getEntityManager().createQuery("select s from Services s where s.ctg.id=:ctgId and s.meta like :meta");
            q.setParameter("meta", "%" + search + "%");
        }else{
            q =  getEntityManager().createQuery("select s from Services s where s.ctg.id=:ctgId");
        }
        q.setParameter("ctgId", id);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        System.out.println(q);
        return q.getResultList();
    }

}
