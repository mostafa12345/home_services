/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.services.api_facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import org.homeServices.model.User;

/**
 *
 * @author davood
 */
@Stateless
@Path("serviceProvider")
public class UserFacadeREST extends AbstractFacade<User> {

    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public UserFacadeREST() {
        super(User.class);
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public User find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<User> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to, @QueryParam("search") String search) {
        return findRange(new int[]{from, to},search);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public List<User> findRange(int[] range,String search) {
        try {
            String sq = "select u from User u where u.type=:type";
            if (search != null) {
                sq = sq + " and (u.name like :name or u.family like :family" +
                        " or u.phone like :phone or u.email like :email" +
                        " or u.fullInfoUser.description like :description)";
            }

            javax.persistence.Query q = getEntityManager().createQuery(sq);
            q.setParameter("type", 2);
            if (search != null) {
                q.setParameter("name", "%" + search + "%");
                q.setParameter("family", "%" + search + "%");
                q.setParameter("phone", "%" + search + "%");
                q.setParameter("email", "%" + search + "%");
                q.setParameter("description", "%" + search + "%");
            }
            System.out.println(q);
            q.setMaxResults(range[1] - range[0] + 1);
            q.setFirstResult(range[0]);
            return q.getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
