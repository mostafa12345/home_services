/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.services.api_facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import org.homeServices.model.CtgServices;

/**
 *
 * @author davood
 */
@Stateless
@Path("category_services")
public class CtgServicesFacadeREST extends AbstractFacade<CtgServices> {

    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public CtgServicesFacadeREST() {
        super(CtgServices.class);
    }

    @GET
    @Path("/{id}")
    @Produces( MediaType.APPLICATION_JSON)
    public CtgServices find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<CtgServices> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CtgServices> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to, @QueryParam("search") String search) {
        if(search == null) {
            return super.findRange(new int[]{from, to});
        }else{
            return  this.findRangeSearch(new int[]{from,to},search);
        }
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    private List<CtgServices> findRangeSearch(int []range,String search){
        javax.persistence.Query q = getEntityManager().createQuery("select c from CtgServices c where c.meta like :meta");
        q.setParameter("meta","%"+search+"%");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
}
