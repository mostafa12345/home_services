/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tempuri;

import javax.jws.WebService;

/**
 *
 * @author davood
 */
@WebService(serviceName = "SendReceive", portName = "SendReceiveSoap", endpointInterface = "org.tempuri.SendReceiveSoap", targetNamespace = "http://tempuri.org/", wsdlLocation = "WEB-INF/wsdl/ip.sms.ir/ws/SendReceive.asmx.wsdl")
public class WebServiceWsdl {

    public void getSMSLines(java.lang.String userName, java.lang.String password, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfSMSLineNumber> getSMSLinesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void saveNewSchedulSendSmsDaily(java.lang.String userName, java.lang.String password, org.tempuri.ScheduleSendDaily dailyScheduleSend, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Long> saveNewSchedulSendSmsDailyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getListOfScheduleSends(java.lang.String userName, java.lang.String password, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfScheduleSend> getListOfScheduleSendsResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void saveNewSchedulSendSmsMonthly(java.lang.String userName, java.lang.String password, org.tempuri.ScheduleSendMonthly monthlyScheduleSend, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Long> saveNewSchedulSendSmsMonthlyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getSendToBranchesSendMethods(java.lang.String userName, java.lang.String password, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfSendToBranchSendMethod> getSendToBranchesSendMethodsResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getBranches(java.lang.String userName, java.lang.String password, int parentBranchID, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfBranch> getBranchesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getSendToBranchFilterConditions(java.lang.String userName, java.lang.String password, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfSendToBranchFilterConditions> getSendToBranchFilterConditionsResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendToParish(java.lang.String userName, java.lang.String password, int smsLineID, int sendCount, int sendMethodID, int startAt, long fromNumber, long toNumber, int filterID, long filterValue, java.lang.String messageBody, int parishID, javax.xml.datatype.XMLGregorianCalendar sendSince, boolean isFlash, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Integer> sendToParishResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendToParishWithBatchkey(java.lang.String userName, java.lang.String password, int smsLineID, int sendCount, int sendMethodID, int startAt, long fromNumber, long toNumber, int filterID, long filterValue, java.lang.String messageBody, int parishID, javax.xml.datatype.XMLGregorianCalendar sendSince, boolean isFlash, javax.xml.ws.Holder<java.lang.String> batchkey, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Integer> sendToParishWithBatchkeyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void saveNewSchedulSendSmsWeekly(java.lang.String userName, java.lang.String password, org.tempuri.ScheduleSendWeekly weeklyScheduleSend, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Long> saveNewSchedulSendSmsWeeklyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void deleteSchedulSendSms(java.lang.String userName, java.lang.String password, int schedulSendSmsID, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Long> deleteSchedulSendSmsResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getSentMessageStatus(java.lang.String userName, java.lang.String password, java.lang.String batchKey, javax.xml.datatype.XMLGregorianCalendar sendDateTime, int requestedPageNumber, int rowsPerPage, javax.xml.ws.Holder<Integer> countOfAll, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfSentSmsDetails> getSentMessageStatusResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getSentMessageStatusByID(java.lang.String userName, java.lang.String password, long messageID, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.SentSmsDetails> getSentMessageStatusByIDResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getSentMessages(java.lang.String userName, java.lang.String password, javax.xml.datatype.XMLGregorianCalendar fromDate, javax.xml.datatype.XMLGregorianCalendar toDate, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfSentMessage> getSentMessagesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getReceivedMessageByLastMessageID(java.lang.String userName, java.lang.String password, long lastMessageID, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfReceivedMessage> getReceivedMessageByLastMessageIDResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getReceivedMessages(java.lang.String userName, java.lang.String password, javax.xml.datatype.XMLGregorianCalendar fromDate, javax.xml.datatype.XMLGregorianCalendar toDate, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfReceivedMessage> getReceivedMessagesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getAllMessages(java.lang.String userName, java.lang.String password, javax.xml.datatype.XMLGregorianCalendar fromDate, javax.xml.datatype.XMLGregorianCalendar toDate, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfAllMessage> getAllMessagesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getTrashedMessages(java.lang.String userName, java.lang.String password, javax.xml.datatype.XMLGregorianCalendar fromDate, javax.xml.datatype.XMLGregorianCalendar toDate, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfTrashedMessage> getTrashedMessagesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public javax.xml.datatype.XMLGregorianCalendar getCurrentDate() {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getUserCredit(java.lang.String userName, java.lang.String password, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<java.lang.Double> getUserCreditResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getDefualtLineNumber(java.lang.String userName, java.lang.String password, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<java.lang.Integer> getDefualtLineNumberResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendMessageCustomerClub(java.lang.String userName, java.lang.String password, java.lang.String messageBody, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Integer> sendMessageCustomerClubResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendMessageToOneCustomerClub(java.lang.String userName, java.lang.String password, java.lang.String messageBody, java.lang.String mobile, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Integer> sendMessageToOneCustomerClubResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendBulkMessageToCustomersClub(java.lang.String userName, java.lang.String password, java.lang.String messageBody, org.tempuri.ArrayOfLong listOfmobile, javax.xml.datatype.XMLGregorianCalendar sendDateTime, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendBulkMessageToCustomersClubResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void vipSendMessages(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfWebServiceSmsSend webServiceSMSSendDetails, java.lang.String lineNumber, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> vipSendMessagesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void vipSendMessageWithBachKey(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfWebServiceSmsSend webServiceSMSSendDetails, java.lang.String lineNumber, javax.xml.ws.Holder<java.lang.String> batchKey, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> vipSendMessageWithBachKeyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getVipSentMessages(java.lang.String userName, java.lang.String password, javax.xml.datatype.XMLGregorianCalendar fromDate, javax.xml.datatype.XMLGregorianCalendar toDate, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfSentMessage> getVipSentMessagesResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getVipSentMessageStatusByID(java.lang.String userName, java.lang.String password, long messageID, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.SentSmsDetails> getVipSentMessageStatusByIDResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getVipSentMessageStatusByIDWithDate(java.lang.String userName, java.lang.String password, long messageID, javax.xml.datatype.XMLGregorianCalendar messageSendDateTime, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.SentSmsDetails> getVipSentMessageStatusByIDWithDateResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void getVipSentMessageStatus(java.lang.String userName, java.lang.String password, java.lang.String batchKey, int requestedPageNumber, int rowsPerPage, javax.xml.ws.Holder<Integer> countOfAll, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfSentSmsDetails> getVipSentMessageStatusResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void vipSendMessageWithLineNumber(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfLong mobileNos, org.tempuri.ArrayOfString messages, java.lang.String lineNumber, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> vipSendMessageWithLineNumberResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void vipSendMessageWithLineNumberAndBatchKey(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfLong mobileNos, org.tempuri.ArrayOfString messages, java.lang.String lineNumber, javax.xml.ws.Holder<java.lang.String> batchKey, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> vipSendMessageWithLineNumberAndBatchKeyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void saveVIPSendCorespondentMessage(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfVIPSendCorespondentMessage vipSendCorespondent, java.lang.String lineNumber, java.lang.String countOfRecipient, java.lang.String cost, int increaseChargeCount, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Boolean> saveVIPSendCorespondentMessageResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void windowsServiceSendAutomaticSMS(java.lang.String userName, java.lang.String password, int lineNumberId, org.tempuri.ArrayOfWebServiceSmsSend webServiceSMSSendDetails, java.lang.String secretCode, int ownerId, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<Boolean> windowsServiceSendAutomaticSMSResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendMessageWithLineNumber(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfLong mobileNos, org.tempuri.ArrayOfString messages, java.lang.String lineNumber, javax.xml.datatype.XMLGregorianCalendar sendDateTime, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendMessageWithLineNumberResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendBulkMessageWithLineNumber(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfLong mobileNos, java.lang.String messages, java.lang.String lineNumber, javax.xml.datatype.XMLGregorianCalendar sendDateTime, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendBulkMessageWithLineNumberResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendBulkMessageWithLineNumberAndBatchKey(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfLong mobileNos, java.lang.String messages, java.lang.String lineNumber, javax.xml.datatype.XMLGregorianCalendar sendDateTime, javax.xml.ws.Holder<java.lang.String> batchKey, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendBulkMessageWithLineNumberAndBatchKeyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendMessageWithLineNumberAndBatchKey(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfLong mobileNos, org.tempuri.ArrayOfString messages, java.lang.String lineNumber, javax.xml.datatype.XMLGregorianCalendar sendDateTime, javax.xml.ws.Holder<java.lang.String> batchKey, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendMessageWithLineNumberAndBatchKeyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendMessage(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfWebServiceSmsSend webServiceSMSSendDetails, int lineNumberID, javax.xml.datatype.XMLGregorianCalendar sendDateTime, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendMessageResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void sendMessageWithBachKey(java.lang.String userName, java.lang.String password, org.tempuri.ArrayOfWebServiceSmsSend webServiceSMSSendDetails, int lineNumberID, javax.xml.datatype.XMLGregorianCalendar sendDateTime, javax.xml.ws.Holder<java.lang.String> batchKey, javax.xml.ws.Holder<java.lang.String> message, javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendMessageWithBachKeyResult) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public java.lang.String sendSmart(java.lang.String userName, java.lang.String password, java.lang.String fileName, byte[] fileContent, javax.xml.datatype.XMLGregorianCalendar sendSince, boolean isFlash, java.lang.String mobileNumColumn, java.lang.String messageText, int smsLineId) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public java.lang.String sendSmartMadiran(java.lang.String userName, java.lang.String password, java.lang.String fileName, byte[] fileContent, javax.xml.datatype.XMLGregorianCalendar sendSince, boolean isFlash, java.lang.String mobileNumColumn, java.lang.String smartSendMessageTemplate, java.lang.String simpleSendMessageTemplate, int smsLineId) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
