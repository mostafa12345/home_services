/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.homeServices.model.Documents;

@Stateless
public class DocumentsFacade extends AbstractFacade<Documents>{
    
    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;    
    public DocumentsFacade() {
        super(Documents.class);
    }
    
    public EntityManager getEntityManager(){
        return em;
    }
    
    @Override
    public void remove(Documents entity) {
        entity.setDeleted(1);
        getEntityManager().merge(entity);
    }
    
}
