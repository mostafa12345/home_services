/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.homeServices.model.CtgServices;

@Stateless
public class CtgServicesFacade extends AbstractFacade<CtgServices>{
    
    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CtgServicesFacade() {
        super(CtgServices.class);
    }
    
}
