/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.homeServices.model.Services;

/**
 *
 * @author davood
 */
@Stateless
public class ServicesFacade extends AbstractFacade<Services>{
    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public ServicesFacade() {
        super(Services.class);
    }

    public EntityManager getEntityManager() {
        return em;
    }
}
