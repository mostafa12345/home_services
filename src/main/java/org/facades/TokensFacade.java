/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.helper.token.Tokens;

@Stateless
public class TokensFacade extends AbstractFacade<Tokens>{
    
    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TokensFacade() {
        super(Tokens.class);
    }
    
    private String issueToken() {
        Random random = new SecureRandom();
        return new BigInteger(400, random).toString(30);
    }

    public String writeToken(String mobile, Integer device,Integer role) {
        try {
            LocalTime now = LocalTime.now();
            String token = issueToken();
            Tokens tokens = new Tokens(token, mobile, device, now.plusMinutes(15).toString(),role);
            em.persist(tokens);
            return token;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public List<Tokens> select() {
        TypedQuery<Tokens> q1 = em.createQuery("SELECT t FROM Tokens t", Tokens.class);
        return q1.getResultList();
    }

    public Tokens findToken(String token) {
        try {
            Tokens tokens = (Tokens) em.createQuery("select t from Tokens t where t.token=:token ").
                    setParameter("token", token).setMaxResults(1).getSingleResult();
            
            if (tokens == null) {
                return null;
            }
            if (tokens.getDevice() == 1) {
                LocalTime old = LocalTime.parse(tokens.getTime());
                LocalTime now = LocalTime.now();
                if (now.isAfter(old)) {
                    em.remove(tokens);
                    return null;
                }
                tokens.setTime(now.plusMinutes(15).toString());
                em.merge(tokens);
            }
            return tokens;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
}
