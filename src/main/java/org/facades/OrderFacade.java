/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.homeServices.model.Order;
import org.homeServices.model.User;

/**
 *
 * @author davood
 */
@Stateless
public class OrderFacade extends AbstractFacade<Order>{
    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public OrderFacade() {
        super(Order.class);
    }

    public EntityManager getEntityManager() {
        return em;
    }
    
    public List<Order> findByUser(User user){
        return em.createQuery("select o from Order o where o.userId=:userId")
                .setParameter("userId", user).getResultList();
    }
}
