/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import java.util.List;
import javax.ejb.Stateless;
import javax.jdo.annotations.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.eclipse.persistence.annotations.ReadOnly;
import org.homeServices.model.User;

@Stateless
public class UserFacade extends AbstractFacade<User> {

    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public UserFacade() {
        super(User.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public User findByMobile(String mobile) {
        try {
            System.out.println("ddddddddddd" + mobile);
            return (User) em.createQuery("select U From User AS U WHERE U.mobile=:mobile").setParameter("mobile", mobile).setMaxResults(1).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public List<User> findAll() {
        String q = "select c from " + entityClass.getCanonicalName().split("\\.")[entityClass.getCanonicalName().split("\\.").length - 1] + " c where c.deleted<>1";
        return getEntityManager().createQuery(q).getResultList();
    }
}
