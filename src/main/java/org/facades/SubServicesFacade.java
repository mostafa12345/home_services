/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.homeServices.model.SubServices;
import org.homeServices.model.User;

@Stateless
public class SubServicesFacade extends AbstractFacade<SubServices> {

    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public SubServicesFacade() {
        super(SubServices.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public List<SubServices> findByUser(User user) {
        try {
            return em.createQuery("select s from ServicesProviders s WHERE s.fldtbluserId = :fldtbluserId")
                    .setParameter("fldtbluserId", user).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
