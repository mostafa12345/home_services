/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.facades;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.homeServices.model.Obligation;

/**
 *
 * @author davood
 */
@Stateless
public class ObligationFacade extends AbstractFacade<Obligation> {

    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    public ObligationFacade() {
        super(Obligation.class);
    }

    public EntityManager getEntityManager() {
        return em;
    }
}
