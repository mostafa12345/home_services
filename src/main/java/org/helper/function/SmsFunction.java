/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.helper.function;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;
import org.tempuri.ArrayOfSentMessage;
import org.tempuri.ArrayOfString;
import org.tempuri.SentSmsDetails;

/**
 *
 * @author davood
 */
public class SmsFunction {

    private static String userName = "9121015428";
    private static String password = "Aban2015";
    private static String lineNumber = "30004505003063";

    public static HashMap<Integer, String> sendSimpleSms(String mobile, String inputMessage) {
        HashMap<Integer, String> result = new HashMap<>();
        Thread t1;
        t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    org.tempuri.SendReceive service = new org.tempuri.SendReceive();
                    org.tempuri.SendReceiveSoap port = service.getSendReceiveSoap();
                    // TODO initialize WS operation arguments here

                    org.tempuri.ArrayOfLong mobileNos = new org.tempuri.ArrayOfLong();
                    mobileNos.getLong().add(Long.parseLong(mobile));
                    org.tempuri.ArrayOfString messages = new org.tempuri.ArrayOfString();
                    GregorianCalendar gregory = new GregorianCalendar();
                    gregory.setTime(new Date());
                    XMLGregorianCalendar sendDateTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
                    javax.xml.ws.Holder<java.lang.String> message = new javax.xml.ws.Holder<>();
                    message.value = inputMessage;
                    messages.getString().add(message.value);
                    javax.xml.ws.Holder<org.tempuri.ArrayOfLong> sendMessageWithLineNumberResult = new javax.xml.ws.Holder<>();
                    port.sendMessageWithLineNumber(userName,
                            password,
                            mobileNos,
                            messages,
                            lineNumber,
                            sendDateTime,
                            message,
                            sendMessageWithLineNumberResult);

                    c = sendMessageWithLineNumberResult.value.getLong().get(0);
                    result.put(0, String.valueOf(c));
                    System.out.println("status is : " + sendMessageWithLineNumberResult.value.getLong().get(0));
                } catch (NumberFormatException | DatatypeConfigurationException ex) {
                    ex.printStackTrace();
                }
            }
        });
        t1.start();

//        Thread t2 = new Thread(() -> {
//
//            try {
//                Thread.sleep(50000);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(SmsFunction.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            org.tempuri.SendReceive service = new org.tempuri.SendReceive();
//            org.tempuri.SendReceiveSoap port = service.getSendReceiveSoap();
//            javax.xml.ws.Holder<java.lang.String> message = new javax.xml.ws.Holder<>();
//            Holder<SentSmsDetails> re = new Holder<>();
//            port.getSentMessageStatusByID(
//                    userName,
//                    password,
//                    c//42551034////42515638//42437263//
//                    , message,
//                    re);
//            System.out.println("delivery status is " + re.value.getDeliveryStatus()
//                    + " ,fatal error : " + re.value.getDeliveryStatusFetchError()
//                    + " ,mobile no : " + re.value.getMobileNo()
//                    + " ,sms body : " + re.value.getSMSMessageBody()
//                    + " ,is need check : " + re.value.isNeedsReCheck());
//            Delivery = re.value.getDeliveryStatus();
//        });
//        t2.start();
//        try {
//            Thread.sleep(50010);
//        } catch (InterruptedException e) {
//            System.err.println("Error : " + e.getMessage());
//        }
//        if (!t2.isAlive()) {
        if (Delivery.isEmpty() || Delivery.equals("()")) {
            Delivery = "-1";
        }

        result.put(1, Delivery);
        return result;
//        }
    }
    public static Long c;
    public static String Delivery = "-1";

    public static String checkWithStatus(String status) {
        org.tempuri.SendReceive service = new org.tempuri.SendReceive();
        org.tempuri.SendReceiveSoap port = service.getSendReceiveSoap();
        javax.xml.ws.Holder<java.lang.String> message = new javax.xml.ws.Holder<>();
        Holder<SentSmsDetails> re = new Holder<>();

        port.getSentMessageStatusByID(
                userName,
                password,
                Long.valueOf(status)//42551034////42515638//42437263//
                , message,
                re);
        if (re.value.getDeliveryStatus().isEmpty() || re.value.getDeliveryStatus().equals("()")) {
            return "-1";
        }
        return re.value.getDeliveryStatus();
    }

}
