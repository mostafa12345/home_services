package org.helper.function;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Properties;
import org.hibernate.Hibernate;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.homeServices.model.Comment;
import org.homeServices.model.CtgServices;
import org.homeServices.model.Documents;
import org.homeServices.model.FullInfoUser;
import org.homeServices.model.Obligation;
import org.homeServices.model.Order;
import org.homeServices.model.Question;
import org.homeServices.model.Rate;
import org.homeServices.model.Services;
import org.homeServices.model.SubServices;
import org.homeServices.model.Tokens;
import org.homeServices.model.User;
import org.homeServices.model.UserAddress;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author gomaneh
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            Configuration conf = new Configuration();

            conf.addAnnotatedClass(CtgServices.class);
            conf.addAnnotatedClass(Comment.class);
            conf.addAnnotatedClass(Documents.class);
            conf.addAnnotatedClass(FullInfoUser.class);

            conf.addAnnotatedClass(Obligation.class);
            conf.addAnnotatedClass(Order.class);
            conf.addAnnotatedClass(Question.class);
            conf.addAnnotatedClass(Rate.class);
            conf.addAnnotatedClass(Services.class);
            
            conf.addAnnotatedClass(SubServices.class);
            
            conf.addAnnotatedClass(Tokens.class);
            conf.addAnnotatedClass(User.class);
            conf.addAnnotatedClass(UserAddress.class);

            conf.setProperties(new Properties() {
                {
                    put("hibernate.connection.username", "root");
                    put("hibernate.connection.password", "");
                    put("hibernate.connection.driver_class",
                            "com.mysql.jdbc.Driver");
                    put("hibernate.connection.url",
                            "jdbc:mysql://localhost:3306/home_service");
                    put("hibernate.enable_lazy_load_no_trans","true");

                }
            });
            return conf.buildSessionFactory(new StandardServiceRegistryBuilder().applySettings(conf.getProperties()).build());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("error create session factory");
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
