package org.helper.token;

import javax.persistence.Column;
import javax.persistence.Entity;
import org.homeServices.model.BaseEntity;

@Entity
public class Tokens extends BaseEntity {
    @Column
    private String token;
    @Column
    private String mobile;
    @Column
    private Integer device;
    @Column
    private String time;
    @Column
    private Integer role;

    public Tokens(String token, String mobile, Integer device, String time, Integer role) {
        this.token = token;
        this.mobile = mobile;
        this.device = device;
        this.time = time;
        this.role = role;
    }

    public Tokens() {
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
   
    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMobile() {
        return mobile;
    }

    public Integer getDevice() {
        return device;
    }

    public void setDevice(Integer device) {
        this.device = device;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Tokens{" + "id=" + getId()+ ", token=" + token + ", mobile=" + mobile + ", device=" + device + ", time=" + time + " ,role=" + role + '}';
    }

}
