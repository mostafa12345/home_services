package org.admin.jsf;

import java.io.File;
import java.io.IOException;
import org.homeServices.model.User;
import org.admin.jsf.util.JsfUtil;
import org.admin.jsf.util.JsfUtil.PersistAction;
import org.facades.UserFacade;

import java.io.Serializable;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.admin.bean.FullInfoUserFacade;
import org.admin.bean.UserAddressFacade;
import org.facades.DocumentsFacade;
import org.helper.function.DocumentsType;
import org.helper.function.UserType;
import org.homeServices.model.Documents;
import org.homeServices.model.FullInfoUser;
import org.homeServices.model.UserAddress;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.w3c.dom.stylesheets.DocumentStyle;

@Named("providerController")
@SessionScoped
public class ProviderController implements Serializable {

    @EJB
    private UserFacade ejbFacade;
    @EJB
    private DocumentsFacade documentsFacade;
    @EJB
    private FullInfoUserFacade fullInfoUserFacade;
    @EJB
    private UserAddressFacade userAddressFacade;
    private List<User> items = null;
    private User selected;

    private FullInfoUser fullInfoUser;

    private List<String> Address;

    private UploadedFile file;

    private UploadedFile shFile;
    private UploadedFile nationalCodeFile;
    private UploadedFile logo;

    private String newAddress;

    private List<UploadedFile> fileListSlider;
    private List<UploadedFile> fileListCertificate;

    public ProviderController() {
    }

    public UploadedFile getShFile() {
        return shFile;
    }

    public void setShFile(UploadedFile shFile) {
        this.shFile = shFile;
    }

    public UploadedFile getNationalCodeFile() {
        return nationalCodeFile;
    }

    public void setNationalCodeFile(UploadedFile nationalCodeFile) {
        this.nationalCodeFile = nationalCodeFile;
    }

    public UploadedFile getLogo() {
        return logo;
    }

    public void setLogo(UploadedFile logo) {
        this.logo = logo;
    }

    public void handlFileUploadSlider(FileUploadEvent event) {
        fileListSlider.add(event.getFile());
        System.err.println("-----------------------");
        System.out.println(fileListSlider);
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void handlFileUploadCertificate(FileUploadEvent event) {
        fileListCertificate.add(event.getFile());
        System.err.println("-----------------------");
        System.out.println(fileListCertificate);
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getNewAddress() {
        return newAddress;
    }

    public void addAddress() {
        this.Address.add(newAddress);
        newAddress = "";
    }

    public void setNewAddress(String newAddress) {
        this.newAddress = newAddress;
    }

    public List<String> getAddress() {
        return Address;
    }

    public void setAddress(List<String> Address) {
        this.Address = Address;
    }

    public FullInfoUser getFullInfoUser() {
        return fullInfoUser;
    }

    public void setFullInfoUser(FullInfoUser fullInfoUser) {
        this.fullInfoUser = fullInfoUser;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public User getSelected() {
        return selected;
    }

    public void setSelected(User selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private UserFacade getFacade() {
        return ejbFacade;
    }

    public User prepareCreate() {
        selected = new User();
        fullInfoUser = new FullInfoUser();
        Address = new ArrayList<>();
        fileListSlider = new ArrayList<>();
        fileListCertificate = new ArrayList<>();
        initializeEmbeddableKey();
        return selected;
    }

    public User prepareEdit() {
        if (selected.getFullInfoUser() == null) {
            selected.setFullInfoUser(new FullInfoUser());
        }
        return selected;
    }

    public void create() {
        //upload portfolio file
        if (file != null && !file.getFileName().isEmpty()) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/home_service/ctg_services");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String fileName = System.currentTimeMillis() + ".png";
            File name = new File(dir.getPath() + "/" + fileName);

            try {
                Files.copy(file.getInputstream(), name.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
                selected.setImage(fileName);
                file = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //set type user
        selected.setType(UserType.SERVICEPROVIDER);
        //create fullinfo user section
        boolean createFullInfo = false;
        if (fullInfoUser.getShShenas() != null && !fullInfoUser.getShShenas().isEmpty()
                && fullInfoUser.getNationalCode() != null && !fullInfoUser.getNationalCode().isEmpty()) {
            fullInfoUserFacade.create(fullInfoUser);
            selected.setFullInfoUser(fullInfoUser);
            createFullInfo = true;
        }
        //create  user
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("UserCreated"));
        System.out.println(selected);

        //upload logo file
        if (logo != null && !logo.getFileName().isEmpty()) {
            if (!createFile(logo, "logo", DocumentsType.LOGO, selected, "")) {
                FacesMessage message = new FacesMessage("Error", "مشکل در آپلود لوگو");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
            logo = null;
        }
        String desc = "";
        //upload shenasname file an save
        if (shFile != null && !shFile.getFileName().isEmpty()) {
            if (createFullInfo) {
                desc = fullInfoUser.getShShenas();
            }
            if (!createFile(shFile, "personal", DocumentsType.PERSONAL, selected, "")) {
                FacesMessage message = new FacesMessage("Error", "مشکل در آپلود اسکن شناسنامه");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
            shFile = null;
        }
        //upload national code scane
        if (nationalCodeFile != null && !nationalCodeFile.getFileName().isEmpty()) {
            if (createFullInfo) {
                desc = fullInfoUser.getNationalCode();
            }
            if (!createFile(nationalCodeFile, "personal", DocumentsType.PERSONAL, selected, desc)) {
                FacesMessage message = new FacesMessage("Error", "مشکل در آپلود اسکن کارت ملی");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
            nationalCodeFile = null;
        }
        //upload certificate 
        if (fileListCertificate != null && fileListCertificate.size() != 0) {
            boolean problem = false;
            for (UploadedFile f : fileListCertificate) {
                if (!createFile(f, "certificate", DocumentsType.CERTIFICATE, selected, "")) {
                    problem = true;
                }
            }
            if (problem) {
                FacesMessage message = new FacesMessage("Error", "مشکل در آپلود گواهینامه ها");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
            fileListCertificate = new ArrayList<>();
        }
        //upload Sliders file
        if (fileListSlider != null && fileListSlider.size() != 0) {
            boolean problem = false;
            for (UploadedFile f : fileListSlider) {
                if (!createFile(f, "slider", DocumentsType.SLIDER, selected, "")) {
                    problem = true;
                }
            }
            if (problem) {
                FacesMessage message = new FacesMessage("Error", "مشکل در آپلود اسلایدر ها");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
            fileListSlider = new ArrayList<>();
        }
        //insert address to database
        if (Address != null && Address.size() != 0) {
            for (String s : Address) {
                UserAddress address = new UserAddress();
                address.setAddress(s);
                address.setUser(selected);
                userAddressFacade.create(address);
            }
        }
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public boolean createFile(UploadedFile inputFile, String folder, Integer type, User user, String desc) {
        String home = System.getProperty("user.home");
        File dir = new File(home + "/home_service/" + folder);
        System.out.println(dir.getPath());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String fileName = System.currentTimeMillis() + "." + inputFile.getFileName().split("\\.")[1];
        File name = new File(dir.getPath() + "/" + fileName);
        Documents document = new Documents();
        try {
            Files.copy(inputFile.getInputstream(), name.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
            document.setFile(fileName);
            document.setUser(user);
            document.setType(type);
            document.setDesc(desc);
            documentsFacade.create(document);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String getTypes(Integer type) {
        if (Objects.equals(type, DocumentsType.CERTIFICATE)) {
            return "گواهینامه";
        } else if (Objects.equals(type, DocumentsType.LOGO)) {
            return "لوگو";
        } else if (Objects.equals(type, DocumentsType.PERSONAL)) {
            return "مدارک شخصی";
        } else if (Objects.equals(type, DocumentsType.SLIDER)) {
            return "تصاویر اسلایدر";
        }
        return "نوع مشخص نیست";
    }

    public String getDocuments(Integer type, String nameFile) {
        if (Objects.equals(type, DocumentsType.CERTIFICATE)) {
            return "certificate/" + nameFile;
        } else if (Objects.equals(type, DocumentsType.LOGO)) {
            return "logo/" + nameFile;
        } else if (Objects.equals(type, DocumentsType.PERSONAL)) {
            return "personal/" + nameFile;
        } else if (Objects.equals(type, DocumentsType.SLIDER)) {
            return "slider/" + nameFile;
        }
        return nameFile;
    }

    public void removeImageFile(Documents d) {
        String home = System.getProperty("user.home");
        try {
            File exists = new File(home + "/home_service/" + getDocuments(d.getType(), d.getFile()));
            if (exists.exists()) {
                exists.delete();
            }
            documentsFacade.remove(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update() {
        if (file != null && !file.getFileName().isEmpty()) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/home_service/profile");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String fileName = System.currentTimeMillis() + ".png";
            File exists = new File(home + "/home_service/profile/" + selected.getImage());
            if (exists.exists()) {
                exists.delete();
            }
            File name = new File(dir.getPath() + "/" + fileName);
            try {
                Files.copy(file.getInputstream(), name.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
                selected.setImage(fileName);
                file = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("UserUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("UserDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<User> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction == PersistAction.UPDATE) {
                    getFacade().edit(selected);
                } else if (persistAction == PersistAction.CREATE) {
                    getFacade().create(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public User getUser(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<User> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<User> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = User.class)
    public static class UserControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UserController controller = (UserController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "userController");
            return controller.getUser(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof User) {
                User o = (User) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), User.class.getName()});
                return null;
            }
        }

    }

}
