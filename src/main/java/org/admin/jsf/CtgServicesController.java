package org.admin.jsf;

import java.io.File;
import java.io.IOException;
import org.homeServices.model.CtgServices;
import org.admin.jsf.util.JsfUtil;
import org.admin.jsf.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.model.UploadedFile;

@Named("ctgServicesController")
@SessionScoped
public class CtgServicesController implements Serializable {

    @EJB
    private org.facades.CtgServicesFacade ejbFacade;
    private List<CtgServices> items = null;
    private CtgServices selected;
    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public CtgServicesController() {
    }

    public CtgServices getSelected() {
        return selected;
    }

    public void setSelected(CtgServices selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private org.facades.CtgServicesFacade getFacade() {
        return ejbFacade;
    }

    public CtgServices prepareCreate() {
        selected = new CtgServices();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        if (file != null) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/home_service/ctg_services");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String fileName = System.currentTimeMillis() + ".png";
            File name = new File(dir.getPath() + "/" + fileName);

            try {
                Files.copy(file.getInputstream(), name.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
                selected.setFile(fileName);
                file = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("CtgServicesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        if (file != null) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/home_service/ctg_services");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String fileName = System.currentTimeMillis() + ".png";
            File exists = new File(home + "/home_service/ctg_services/"+selected.getFile());
            if(exists.exists()){
                exists.delete();
            }
            File name = new File(dir.getPath() + "/" + fileName);
            try {
                Files.copy(file.getInputstream(), name.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
                selected.setFile(fileName);
                file = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("CtgServicesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("CtgServicesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<CtgServices> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public CtgServices getCtgServices(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<CtgServices> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<CtgServices> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = CtgServices.class)
    public static class CtgServicesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CtgServicesController controller = (CtgServicesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "ctgServicesController");
            return controller.getCtgServices(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof CtgServices) {
                CtgServices o = (CtgServices) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), CtgServices.class.getName()});
                return null;
            }
        }

    }

}
