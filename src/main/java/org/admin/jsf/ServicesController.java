package org.admin.jsf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import org.homeServices.model.Services;
import org.admin.jsf.util.JsfUtil;
import org.admin.jsf.util.JsfUtil.PersistAction;
import org.facades.ServicesFacade;

import java.io.Serializable;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.imageio.stream.FileImageOutputStream;
import net.coobird.thumbnailator.Thumbnails;
import org.facades.DocumentsFacade;
import org.helper.function.DocumentsType;
import org.homeServices.model.Documents;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.UploadedFile;

@Named("servicesController")
@SessionScoped
public class ServicesController implements Serializable {

    @EJB
    private ServicesFacade ejbFacade;
    @EJB
    private DocumentsFacade documentsFacade;
    private List<Services> items = null;
    private Services selected;
    private UploadedFile file;
    private List<UploadedFile> fileListSlider;
    private List<UploadedFile> fileListCertificate;
    private List<Services> itemsActive;
    private List<Services> itemsDisable;

    private CroppedImage croppedImage;
    private Integer fileType;
    private String fileDesc;

    public List<Documents> getDocuments() {
        return documents;
    }
    
    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }
    
    private List<Documents> documents;

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public CroppedImage getCroppedImage() {
        return croppedImage;
    }

    public void setCroppedImage(CroppedImage croppedImage) {
        this.croppedImage = croppedImage;
    }

    public void crop() {
        if (croppedImage == null) {
            return;
        }
        String home = System.getProperty("user.home");
        File dir;
        if (fileType == 1) {
            dir = new File(home + "/home_service/services");
        } else if (fileType.equals(DocumentsType.CERTIFICATE)) {
            dir = new File(home + "/home_service/certificate");
        } else {
            dir = new File(home + "/home_service/slider");
        }

        if (!dir.exists()) {
            dir.mkdirs();
        }
        String name = System.currentTimeMillis() + "." + croppedImage.getOriginalFilename().split("\\.")[1];
        File upload = new File(dir.toPath() + "/" + name);

        FileImageOutputStream imageOutput;
        try {
            imageOutput = new FileImageOutputStream(upload);
            imageOutput.write(croppedImage.getBytes(), 0, croppedImage.getBytes().length);
            imageOutput.close();
            Thumbnails.of(upload).size(200, 200).toFile(new File(dir.toPath() + "/thumb" + name));
            System.out.println("org.admin.jsf.ServicesController.crop() file type "+fileType);
            System.out.println("org.admin.jsf.ServicesController.crop() equal "+fileType.equals(DocumentsType.CERTIFICATE));
            if (fileType == 1) {
                selected.setFile(name);
            } else if (fileType.equals(DocumentsType.CERTIFICATE)) {
                Documents d = new Documents();
                d.setFile(name);
                d.setType(DocumentsType.CERTIFICATE);
                d.setDesc(fileDesc);
                documents.add(d);
            } else {
                Documents d = new Documents();
                d.setFile(name);
                d.setType(DocumentsType.SLIDER);
                d.setDesc(fileDesc);
                documents.add(d);
            }

            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            File deleted = new File(externalContext.getRealPath("") + File.separator + "resources" + File.separator + "temp"
                    + File.separator + "images" + File.separator + fileName);
            if (deleted.exists()) {
                dir.delete();
            }
            fileName = "";
            croppedImage = null;
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Cropping failed."));
            return;
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Cropping finished."));
    }

    public List<Services> getItemsActive() {
        if (items == null) {
            items = getFacade().findAll();
        }
        itemsActive = new ArrayList<>();
        items.stream().parallel().forEach((s1) -> {
            if (s1.isActive()) {
                itemsActive.add(s1);
            }
        });
        return itemsActive;
    }

    public void setItemsActive(List<Services> itemsActive) {
        this.itemsActive = itemsActive;
    }

    public List<Services> getItemsDisable() {

        if (items == null) {
            items = getFacade().findAll();
        }
        itemsDisable = new ArrayList<>();
        items.stream().parallel().forEach((s1) -> {
            if (!s1.isActive()) {
                itemsDisable.add(s1);
            }
        });
        return itemsDisable;
    }

    public void setItemsDisable(List<Services> itemsDisable) {
        this.itemsDisable = itemsDisable;
    }
    private String fileName = "";

    public String getFileName() {
        return fileName;
    }

    public void handlFile(FileUploadEvent event) {
        file = event.getFile();

        String name = System.currentTimeMillis() + "." + file.getFileName().split("\\.")[1];
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        File dir = new File(externalContext.getRealPath("") + File.separator + "resources" + File.separator + "temp"
                + File.separator + "images");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        
        File upload = new File(dir + "/" + name);
        try {
            Files.copy(file.getInputstream(), upload.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
            fileName = name;
            file = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlFileUploadSlider(FileUploadEvent event) {
        fileListSlider.add(event.getFile());
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void handlFileUploadCertificate(FileUploadEvent event) {
        fileListCertificate.add(event.getFile());
        System.err.println("-----------------------");
        System.out.println(fileListCertificate);
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getTypes(Integer type) {
        if (Objects.equals(type, DocumentsType.CERTIFICATE)) {
            return "گواهینامه";
        } else if (Objects.equals(type, DocumentsType.LOGO)) {
            return "لوگو";
        } else if (Objects.equals(type, DocumentsType.PERSONAL)) {
            return "مدارک شخصی";
        } else if (Objects.equals(type, DocumentsType.SLIDER)) {
            return "تصاویر اسلایدر";
        }
        return "نوع مشخص نیست";
    }

    public String getDocuments(Integer type, String nameFile) {
        if (Objects.equals(type, DocumentsType.CERTIFICATE)) {
            return "certificate/" + nameFile;
        } else if (Objects.equals(type, DocumentsType.LOGO)) {
            return "logo/" + nameFile;
        } else if (Objects.equals(type, DocumentsType.PERSONAL)) {
            return "personal/" + nameFile;
        } else if (Objects.equals(type, DocumentsType.SLIDER)) {
            return "slider/" + nameFile;
        }
        return nameFile;
    }
    
    public String getThumbDocuments(Integer type, String nameFile) {
        if (Objects.equals(type, DocumentsType.CERTIFICATE)) {
            return "certificate/thumb" + nameFile;
        } else if (Objects.equals(type, DocumentsType.LOGO)) {
            return "logo/thumb" + nameFile;
        } else if (Objects.equals(type, DocumentsType.PERSONAL)) {
            return "personal/thumb" + nameFile;
        } else if (Objects.equals(type, DocumentsType.SLIDER)) {
            return "slider/thumb" + nameFile;
        }
        return nameFile;
    }
    
    public void removeImageFile(Documents d) {
        String home = System.getProperty("user.home");
        try {
            File exists = new File(home + "/home_service/" + getDocuments(d.getType(), d.getFile()));
            if (exists.exists()) {
                exists.delete();
            }
            documentsFacade.remove(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void removeSelectedImage(){
        if(selected.getFile() != null && !this.selected.getFile().isEmpty()){
             String home = System.getProperty("user.home");
        try {
            File exists = new File(home + "/home_service/services/" + selected.getFile());
            if (exists.exists()) {
                exists.delete();
            }
            selected.setFile(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        }
    }
    public void cancel(){
        removeSelectedImage();
        for(Documents d: documents){
            removeImage(d);
        }
    }
    
    public void removeImage(Documents d) {
        String home = System.getProperty("user.home");
        try {
            File exists = new File(home + "/home_service/" + getDocuments(d.getType(), d.getFile()));
            if (exists.exists()) {
                exists.delete();
            }
            documents.remove(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public ServicesController() {
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
        System.out.println("set files name is : " + this.file.getFileName());
    }

    public Services getSelected() {
        return selected;
    }

    public void setSelected(Services selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ServicesFacade getFacade() {
        return ejbFacade;
    }

    public Services prepareCreate() {
        selected = new Services();
        fileListSlider = new ArrayList<>();
        fileListCertificate = new ArrayList<>();
        documents = new LinkedList<>();
        initializeEmbeddableKey();
        fileName = "";
        return selected;
    }

    public void prepareUpdate() {
        fileListSlider = new ArrayList<>();
        fileListCertificate = new ArrayList<>();
        fileName = "";
    }

    public void create() {

        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ServicesCreated"));
        System.err.println("-----------------------");
        System.out.println("is empty "+documents.isEmpty());
        System.out.println("org.admin.jsf.ServicesController.create() "+documents.size());
        if(!documents.isEmpty()){
            for(Documents d:documents){
                System.err.println("----------------");
                System.out.println("name file "+d.getFile());
                d.setServices(selected);
                documentsFacade.create(d);
            }
        }

        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public boolean createFile(UploadedFile inputFile, String folder, Integer type, Services services, String desc) {
        String home = System.getProperty("user.home");
        File dir = new File(home + "/home_service/" + folder);
        System.out.println(dir.getPath());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String fileName = System.currentTimeMillis() + "." + inputFile.getFileName().split("\\.")[1];
        File name = new File(dir.getPath() + "/" + fileName);
        Documents document = new Documents();
        try {
            Files.copy(inputFile.getInputstream(), name.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
            document.setFile(fileName);
            document.setServices(services);
            document.setType(type);
            document.setDesc(desc);
            documentsFacade.create(document);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void update() {
        if (file != null) {
            String home = System.getProperty("user.home");
            File dir = new File(home + "/home_service/services");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String fileName = System.currentTimeMillis() + ".png";
            File exists = new File(home + "/home_service/services/" + selected.getFile());
            if (exists.exists()) {
                exists.delete();
            }
            File name = new File(dir.getPath() + "/" + fileName);
            try {
                Files.copy(file.getInputstream(), name.toPath(), new CopyOption[]{StandardCopyOption.REPLACE_EXISTING});
                selected.setFile(fileName);
                file = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (fileListCertificate != null && fileListCertificate.size() != 0) {
            boolean problem = false;
            for (UploadedFile f : fileListCertificate) {
                if (!createFile(f, "certificate", DocumentsType.CERTIFICATE, selected, "")) {
                    problem = true;
                }
            }
            if (problem) {
                FacesMessage message = new FacesMessage("Error", "مشکل در آپلود گواهینامه ها");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
            fileListCertificate = new ArrayList<>();
        }
        //upload Sliders file
        if (fileListSlider != null && fileListSlider.size() != 0) {
            boolean problem = false;
            for (UploadedFile f : fileListSlider) {
                if (!createFile(f, "slider", DocumentsType.SLIDER, selected, "")) {
                    problem = true;
                }
            }
            if (problem) {
                FacesMessage message = new FacesMessage("Error", "مشکل در آپلود اسلایدر ها");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
            fileListSlider = new ArrayList<>();
        }

        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ServicesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ServicesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Services> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction == PersistAction.CREATE) {
                    getFacade().create(selected);
                } else if (persistAction == PersistAction.UPDATE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Services getServices(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Services> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Services> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Services.class)
    public static class ServicesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ServicesController controller = (ServicesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "servicesController");
            return controller.getServices(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Services) {
                Services o = (Services) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Services.class.getName()});
                return null;
            }
        }

    }

}
