/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.admin.bean;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.homeServices.model.UserAddress;

/**
 *
 * @author gomaneh
 */
@Stateless
public class UserAddressFacade extends AbstractFacade<UserAddress> {

    @PersistenceContext(unitName = "homeServicesUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserAddressFacade() {
        super(UserAddress.class);
    }
    
}
