/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.homeServices.filter;

import java.security.Principal;
import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import org.facades.TokensFacade;
import org.helper.token.Tokens;
import org.homeServices.annotation.Secured;
import org.homeServices.model.Credentials;
import org.homeServices.model.Role;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
    
    @Override
    public void filter(ContainerRequestContext requestContext) {
        // Get the HTTP Authorization header from the request
        String authorizationHeader
                = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly 
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        // Extract the token from the HTTP Authorization header
        String token = authorizationHeader.substring("Bearer".length()).trim();
//        System.out.println("---token : "+token);
        try {
            // Validate the token
            final Credentials user = validateToken(token);
            if (user == null) {
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            }
            
            final SecurityContext currentSecurityContext = requestContext.getSecurityContext();
            requestContext.setSecurityContext(new SecurityContext() {
                @Override
                public Principal getUserPrincipal() {
                    return new Principal() {
                        @Override
                        public String getName() {
                            return user.getMobile();
                        }
                    };
                }
                
                @Override
                public boolean isUserInRole(String role) {
                    if (getRoles().toString().equalsIgnoreCase(role)) {
                        return true;
                    }
                    return false;
                }
                
                @Override
                public boolean isSecure() {
                    return currentSecurityContext.isSecure();
                }
                
                @Override
                public String getAuthenticationScheme() {
                    return "Bearer";
                }
                
                public Role getRoles() {
                    return user.getRoles();
                }
            });
        } catch (Exception e) {
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED).build());
            e.printStackTrace();
        }
    }
    
    @EJB
    private TokensFacade tokensFacade;
    
    private Credentials validateToken(String token) throws Exception {
//        System.out.println("org.homeServices.filter.AuthenticationFilter.validateToken() "+token);
        Tokens t = tokensFacade.findToken(token);
        if (t == null) {
            return null;
        }
        Credentials c = new Credentials();
        c.setMobile(t.getMobile());
        c.setDevice(t.getDevice());
        c.setToken(token);
        if (t.getRole() == null || t.getRole() == 0) {
            c.setRoles(Role.ROLE_1);
        } else if (t.getRole() == 1) {
            c.setRoles(Role.ROLE_2);
        } else if (t.getRole() == 2) {
            c.setRoles(Role.ROLE_3);
        } else {
            c.setRoles(Role.ROLE_1);
        }
        
        return c;
    }
}
