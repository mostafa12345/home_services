package org.homeServices.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.facades.TokensFacade;
import org.facades.UserFacade;
import org.helper.function.Messages;
import org.helper.function.SmsFunction;
import org.helper.token.Tokens;
import org.homeServices.model.User;

/**
 *
 * @author davood
 */
@Path("/authentication")
public class AuthController {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Messages authenticateUser(@FormParam("mobile") String mobile,
            @FormParam("password") String password,
            @FormParam("device") Integer device) {
        Messages response = new Messages();
        try {
            if (mobile == null
                    || mobile.isEmpty()
                    || password == null
                    || password.isEmpty()) {
                response.type = "error";
                response.message = "شماره موبایل و رمز عبور نمی تواند خالی باشد";
                response.data = null;
                return response;
            }

            User u = userFacade.findByMobile(mobile);
            if (u == null) {
                response.type = "error";
                response.message = "اطلاعات وارد شده صحیح نمی باشد";
                response.data = null;
                return response;
            }
            if (!u.checkPassword(password)) {
                response.type = "error";
                response.message = "اطلاعات وارد شده صحیح نمی باشد";
                response.data = null;
                return response;
            }
            if (!u.getActive()) {
                response.type = "error";
                response.message = "حساب کاربری شما فعال نمی باشد لطفا کد فعال سازی را ارسال نمائید";
                response.data = null;
                return response;
            }
            if (u.getLock()) {
                response.type = "error";
                response.message = "حساب کاربری شما قفل شده است";
                response.data = null;
                return response;
            }
            // Authenticate the user using the credentials provided
            // Issue a token for the user
            if (device == null) {
                device = 0;
            }
            String token = tokenFacade.writeToken(mobile, device, u.getType());

            response.type = "success";
            response.message = "شما با موفقیت وارد شدید";
            response.data = convertUserToMap(u, token);
            System.out.println(response.data);
            // Return the token on the response
            return response;
        } catch (Exception e) {
            response.type = "error";
            response.message = "اطلاعات وارد شده صحیح نمی باشد";
            response.data = null;
            return response;
        }
    }
    @EJB
    private TokensFacade tokenFacade;

    private String issueToken(String username) {
        Random random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

    @EJB
    private UserFacade userFacade;

    @POST
    @Path("/user/create")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf8")
    @Produces(MediaType.APPLICATION_JSON)
    public Messages createUser(User entity) {
        Messages response = new Messages();
        //check mobile and password is not empty
        if (entity.getMobile() == null
                || entity.getMobile().isEmpty()
                || entity.getPassword() == null
                || entity.getPassword().isEmpty()) {
            response.type = "error";
            response.message = "شماره موبایل و رمز عبور نمی تواند خالی باشد";
            return response;
        }
        Random r = new Random();
        //check is first time to register
        User existEntity = userFacade.findByMobile(entity.getMobile());
        if (existEntity != null) {
            if (existEntity.getActive()) {
                response.type = "error";
                response.message = "شماره شما قبلا در سامانه ثبت شده است";
                return response;
            } else {
                existEntity.setToken(String.valueOf(r.nextInt(1000000)));
//                SmsFunction.sendSimpleSms(existEntity.getMobile(), "کد فعال سازی شما :" + existEntity.getToken());
                existEntity.setSetTokenDate(new Date());
                userFacade.edit(existEntity);
                response.type = "success";
                response.message = "اطلاعات شما با موفقیت ثبت شد و کد فعال سازی برای شما ارسال گردید";
                return response;
            }
        }
        entity.setActive(false);
        entity.setLock(false);
        entity.setToken(String.valueOf(r.nextInt(1000000)));
        //send activate code with sms
//        SmsFunction.sendSimpleSms(entity.getMobile(), "کد فعال سازی شما :" + entity.getToken());
        entity.setSetTokenDate(new Date());
        userFacade.create(entity);
        response.type = "success";
        response.message = "اطلاعات شما با موفقیت ثبت شد و کد فعال سازی برای شما ارسال گردید";
        return response;
    }

    @POST
    @Path("/user/activate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages sendActivateCode(User c) {
        Messages response = new Messages();
        //check token and mobile is not null or empty
        if (c.getMobile() == null
                || c.getMobile().isEmpty()
                || c.getToken() == null
                || c.getToken().isEmpty()) {
            response.type = "error";
            response.message = "شماره موبایل یا کد فعال سازی نمی تواند خالی باشد";
            return response;
        }
        //find user by mobile
        User user = userFacade.findByMobile(c.getMobile());
        //check user
        if (user == null) {
            response.type = "error";
            response.message = "شماره موبایل یا کد فعال سازی صحیح نمی باشد";
            return response;
        }
        //check token code
        if (user.getToken().equals(c.getToken())) {
            //active userW
            user.setActive(true);
            user.setLock(false);
            userFacade.edit(user);
            response.type = "success";
            response.message = "کد فعال سازی صحیح می باشد پنل کاربری شما فعال شد";
            String token = tokenFacade.writeToken(user.getMobile(), 0, user.getType());
            response.data = convertUserToMap(user, token);
            return response;
        }
        response.type = "error";
        response.message = " کد فعال سازی صحیح نمی باشد";
        return response;
    }

    @POST
    @Path("/user/verify_req_pass_token")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages verifyReqPassToken(User c) {
        Messages response = new Messages();
        //check token and mobile is not null or empty
        if (c.getMobile() == null
                || c.getMobile().isEmpty()
                || c.getToken() == null
                || c.getToken().isEmpty()) {
            response.type = "error";
            response.message = "شماره موبایل و کد بازیابی نمی تواند خالی باشد";
            return response;
        }
        //find user by mobile
        User user = userFacade.findByMobile(c.getMobile());
        //check user
        if (user == null) {
            response.type = "error";
            response.message = "شماره موبایل و کد بازیابی صحیح نمی باشد";
            return response;
        }
        //check token code
        if (user.getToken().equals(c.getToken())) {
            user.setToken(issueToken(c.getMobile()));
            userFacade.edit(user);
            response.type = "success";
            response.message = "رمز عبور خود را تغییر دهید";
            Map<String, String> map = new HashMap<>();
            map.put("token", user.getToken());
            response.data = map;
            return response;
        }
        response.type = "error";
        response.message = " کد بازیابی صحیح نمی باشد";
        return response;
    }

    @POST
    @Path("/user/change_password")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf8")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf8")
    public Messages changePassword(User c) {
        Messages response = new Messages();
        //check token and mobile is not null or empty
        if (c.getMobile() == null
                || c.getMobile().isEmpty()
                || c.getToken() == null
                || c.getToken().isEmpty()) {
            response.type = "error";
            response.message = "شماره موبایل و کد بازیابی نمی تواند خالی باشد";
            return response;
        }
        //find user by mobile
        User user = userFacade.findByMobile(c.getMobile());
        //check user
        if (user == null) {
            response.type = "error";
            response.message = "شماره موبایل و کد بازیابی صحیح نمی باشد";
            return response;
        }
        //check token code
        System.out.println(c.getPassword());
        if (user.getToken().equals(c.getToken())) {
            user.setToken(null);
            user.setPlainPassword(c.getPassword());
            userFacade.edit(user);
            response.type = "success";
            response.message = "رمز عبور شما تغییر کرد";
            String token = tokenFacade.writeToken(user.getMobile(), 0, user.getType());
            response.data = convertUserToMap(user, token);
            return response;
        }
        response.type = "error";
        response.message = " اطلاعات شما صحیح نمی باشد";
        return response;
    }

    @POST
    @Path("/user/resend")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages reSendCode(@FormParam("mobile") String mobile) {
        Messages response = new Messages();
        //check mobile and password is not empty
        if (mobile == null || mobile.isEmpty()) {
            response.type = "error";
            response.message = "اطلاعات شما صحیح نیست";
            return response;//"mobile can,t be null";//"شماره موبایل نمی تواند خالی باشد";
        }
        //find user
        User entity = userFacade.findByMobile(mobile);
        if (entity == null) {
            response.type = "error";
            response.message = "اطلاعات شما صحیح نیست";
            return response;//"dont exist user by this mobile";//"شماره شما قبلا در سامانه ثبت شده است";
        }
        Random r = new Random();
        entity.setToken(String.valueOf(r.nextInt(899999) + 100000));
        entity.setActive(false);
        //send activate code with sms
//        SmsFunction.sendSimpleSms(entity.getMobile(), "کد فعال سازی شما :" + entity.getToken());
        entity.setSetTokenDate(new Date());
        userFacade.edit(entity);
        response.type = "success";
        response.message = "کد جدید برای شما ارسال شد";
        return response;
    }

    @GET
    @Path("/user/find/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public User find(@PathParam("id") Integer id) {
        return userFacade.find(id);
    }
    @EJB
    private TokensFacade tokensFacade;

    @GET
    @Path("/tokens")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tokens> getToken() {
        tokensFacade.writeToken("09373298343", 0, 0);
        return tokensFacade.findAll();
    }

    private String convertUserToMap(User user, String token) {
        ObjectMapper mapper = new ObjectMapper();
        User u = new User();
        u.setToken(token);
        u.setName(user.getName());
        u.setFamily(user.getFamily());
        u.setMobile(user.getMobile());
        u.setEmail(user.getEmail());
        u.setPhone(user.getPhone());
        u.setPhone(user.getPhone());
        try {
            return mapper.writeValueAsString(u);
        } catch (JsonProcessingException ex) {
            ex.fillInStackTrace();
        }
        return null;
    }
}
