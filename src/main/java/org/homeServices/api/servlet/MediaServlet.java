/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.homeServices.api.servlet;

import java.io.File;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import org.omnifaces.servlet.FileServlet;

/**
 *
 * @author gomaneh
 */
@WebServlet(name = "MediaServlet", urlPatterns = {"/media/*"})
public class MediaServlet extends FileServlet {
private File folder;

    @Override
    public void init() throws ServletException {
        String home=System.getProperty("user.home");
        folder = new File(home+"/home_service");
    }

    @Override
    protected File getFile(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        System.out.println("sssssssssssssssssssssss");
        if (pathInfo == null || pathInfo.isEmpty() || "/".equals(pathInfo)) {
            throw new IllegalArgumentException();
        }

        return new File(folder, pathInfo);
    }

}
