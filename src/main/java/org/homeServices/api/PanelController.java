package org.homeServices.api;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import org.facades.DocumentsFacade;
import org.facades.OrderFacade;
import org.facades.SubServicesFacade;
import org.facades.UserFacade;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.helper.function.Messages;
import org.homeServices.annotation.Secured;
import org.homeServices.model.Documents;
import org.homeServices.model.Order;
import org.homeServices.model.SubServices;
import org.homeServices.model.User;
import org.homeServices.model.UserAddress;

@Path("/panel")
@Stateless
@Secured
public class PanelController {

    @EJB
    private UserFacade userFacade;
    @Context
    private SecurityContext securityContext;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/user/info")
    public User getProfile() {
        User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
        return user;
    }

    @POST
    @Consumes( MediaType.APPLICATION_JSON)
    public Messages edit(User entity) {
        Messages response = new Messages();
        User findUser = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
        //check data sends
        if (findUser == null) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }
        //check mobile sends and mobile login with him its equle
        if (!entity.getMobile().equals(securityContext.getUserPrincipal().getName())) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }
        //check name and family is not empty
        if (entity.getName() == null && entity.getFamily() == null) {
            response.message = "فیلد نام و نام خانوادگی نمی تواند خالی باشد";
            response.type = "error";
            return response;
        }
        //try to edit user
        try {
            findUser.setName(entity.getName());
            findUser.setFamily(entity.getFamily());
            findUser.setEmail(entity.getEmail());
            findUser.setPhone(entity.getPhone());
            userFacade.edit(findUser);
            response.type = "success";
            response.message = "اطلاعات با موفقیت ویرایش شد";
            return response;
            //if has error
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "مشکل در ویرایش اطلاعات لطفا دوباره تلاش کنید";
            return response;
        }
    }

    @POST
    @Path("/edit-password")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Messages editPassword(User entity) {
        Messages response = new Messages();
        if (entity == null) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }

        User user = userFacade.findByMobile(entity.getMobile());
        if (user == null || !user.getMobile().equals(securityContext.getUserPrincipal().getName())) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }
        if (!user.checkPassword(entity.getOldPassword())) {
            response.type = "error";
            response.message = "رمز عبور وارد شده معتبر نیست.";
            return response;
        }
        try {
            user.setPassword(entity.getPassword());
            userFacade.edit(user);
            response.type = "success";
            response.message = "رمز عبور با موفقیت ویرایش شد";
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "مشکل در ویرایش اطلاعات لطفا دوباره تلاش کنید";
            return response;
        }
    }
    @EJB
    private OrderFacade orderFacade;

    @GET
    @Path("/order")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> getOrders() {
        User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
        if (user == null) {
            return new ArrayList<>();
        }
        return orderFacade.findByUser(user);
    }

    @POST
    @Path("/order")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Messages order(Order order) {
        Messages response = new Messages();
        if (order.getAddress() == null || order.getAddress().isEmpty()
                || order.getDate() == null
                || order.getTime() == null || order.getTime().isEmpty()) {
            response.type = "error";
            response.message = "مقادیر آدرس و تاریخ و زمان نمی تواند خالی باشد";
            return response;
        }
        if (order.getSubServices()== null) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }
        try {
            User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
            order.setUserId(user);
            orderFacade.create(order);
            response.type = "success";
            response.message = "اطلاعات با موفقیت ثبت شد";
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "مشکل در ثبت اطلاعات لطفا دوباره تلاش کنید";
            return response;
        }
    }

    @POST
    @Path("/order/edit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Messages editOrder(Order order) {
        Messages response = new Messages();;
        if (order.getAddress() == null || order.getAddress().isEmpty()
                || order.getDate() == null
                || order.getTime() == null || order.getTime().isEmpty()) {
            response.type = "error";
            response.message = "مقادیر آدرس و تاریخ و زمان نمی تواند خالی باشد";
            return response;
        }
        if (order.getSubServices()== null) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }

        try {
            User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
            order.setUserId(user);
            orderFacade.edit(order);
            response.type = "success";
            response.message = "اطلاعات با موفقیت ویرایش شد";
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "مشکل در ویرایش اطلاعات لطفا دوباره تلاش کنید";
            return response;
        }
    }

    @EJB
    private SubServicesFacade serviceProviderFacade;

    @POST
    @Path("/user/provider")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages insertProvider(User entity) {
        Messages response = new Messages();

        //check is first time to register
        User existEntity = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
        if (existEntity == null) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }
        if (!existEntity.getActive()) {
            response.type = "error";
            response.message = "حساب کاربری شما فعال نشده است";
            return response;
        }
        if (entity.getFullInfoUser().getShShenas() == null || entity.getFullInfoUser().getShShenas().isEmpty()
                || entity.getFullInfoUser().getNationalCode() == null || entity.getFullInfoUser().getNationalCode().isEmpty()
                || entity.getAddress() == null || entity.getAddress().isEmpty()) {
            response.type = "error";
            response.message = "فیلدهای نام و نام خانوادگی و کد ملی و شماره شناسنامه وآدرس نمی تواند خالی باشد";
            return response;
        }

        try {
            userFacade.edit(existEntity);
            response.type = "success";
            response.message = "اطلاعات شما با موفقیت ثبت شد و کد فعال سازی برای شما ارسال گردید";
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "مشکل در ویرایش اطلاعات لطفا دوباره تلاش کنید";
            return response;
        }
    }

    @GET
    @Path("/user/providers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<SubServices> getServicesProviders() {
        List<SubServices> list = new ArrayList<>();
        try {
            User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
            list = serviceProviderFacade.findByUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @POST
    @Path("/user/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages uploadProfile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail,
            @FormDataParam("type") Integer type,
            @FormDataParam("expired") String expired ) {
        Messages response = new Messages();
        if (uploadedInputStream == null) {
            response.type = "error";
            response.message = "فایل ارسال نشده است";
            return response;
        }
        try {
            User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
            if (user == null) {
                response.type = "error";
                response.message = "اطلاعات ارسال شده صحیح نیست";
                return response;
            }
            
            File dir = new File("/opt/upload/"+user.getId()+"/");
            if(!dir.exists()){
                dir.mkdirs();
            }
            
            String sec = fileDetail.getFileName();
            String []f2 = sec.split("\\.");
            if(f2 == null && f2.length == 0){
                response.type = "error";
                response.message = "اطلاعات ارسال شده صحیح نیست";
                return response;
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
            String fileName = sdf.format(new Date())+"."+f2[f2.length - 1];

            // save it
            Files.copy(uploadedInputStream, new File(dir.getPath()+"/"+fileName).toPath(), StandardCopyOption.REPLACE_EXISTING);
            Documents document = new Documents();
            document.setFile(fileName);
            document.setType(type);
            document.setUser(user);
            if(type == 2){
                document.setExpired(expired);
            }
            documentsFacade.create(document);
            response.type = "success";
            response.message = "اطلاعات با موفقیت ثبت شد";
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "مشکل در آپلود عکس لطفا مجددا تلاش کنید";
            return response;
        }
    }
    @EJB
    private DocumentsFacade documentsFacade;
     @POST
    @Path("/upload-image-profile")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages uploadImageProfile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        Messages response = new Messages();
        if (uploadedInputStream == null) {
            response.type = "error";
            response.message = "فایل ارسال نشده است";
            return response;
        }
        try {
            String home = System.getProperty("user.home");
            User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
            if (user == null) {
                response.type = "error";
                response.message = "اطلاعات ارسال شده صحیح نیست";
                return response;
            }
            String filename = user.getMobile() + "_" + System.currentTimeMillis() + "." + getExtension(fileDetail.getFileName());
            String uploadedFileLocation = home + "/home_service/profile";
            File dir = new File(uploadedFileLocation);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            // save it
            Files.copy(uploadedInputStream, new File(uploadedFileLocation + "/" + filename).toPath(), StandardCopyOption.REPLACE_EXISTING);
            File oldFile = new File(uploadedFileLocation + "/" + user.getImage());
            if (oldFile.exists()) {
                oldFile.delete();
            }
            user.setImage(filename);
            userFacade.edit(user);
            response.type = "success";
            response.message = "اطلاعات با موفقیت ثبت شد";
            response.data = filename;
            return response;
        } catch (Exception e) {
            response.type = "error";
            response.message = "مشکل در آپلود عکس لطفا مجددا تلاش کنید";
            return response;
        }
    }
    
    @GET
    @Path("/remove-image-profile")
    @Produces(MediaType.APPLICATION_JSON)
    public Messages removeImageProfile() {
        Messages response = new Messages();
        try {
            String home = System.getProperty("user.home");
            User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
            if (user == null) {
                response.type = "error";
                response.message = "اطلاعات ارسال شده صحیح نیست";
                return response;
}
            String uploadedFileLocation = home + "/home_service/profile";
            File oldFile = new File(uploadedFileLocation + "/" + user.getImage());
            System.out.println(oldFile.getAbsolutePath());
            if (oldFile.exists()) {
                oldFile.delete();
            }
            user.setImage(null);
            userFacade.edit(user);
            response.type = "success";
            response.message = "اطلاعات با موفقیت ثبت شد";
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "خطا در عملیات سرویس دهنده!";
            return response;
        }
    }

    @GET
    @Path("/address/{mobile}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<UserAddress> editAddresses(@PathParam("mobile") String mobile) {

        //check is first time to register
        User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
        if (user == null || !mobile.equals(securityContext.getUserPrincipal().getName())) {
            return new ArrayList();
        }
        return user.getAddress();
    }

    @PUT
    @Path("/edit-addresses/{mobile}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Messages editAddresses(@PathParam("mobile") String mobile, List<UserAddress> entities) {
        Messages response = new Messages();

        //check is first time to register
        User user = userFacade.findByMobile(securityContext.getUserPrincipal().getName());
        if (user == null || !mobile.equals(securityContext.getUserPrincipal().getName())) {
            response.type = "error";
            response.message = "اطلاعات ارسال شده صحیح نیست";
            return response;
        }

        entities.forEach((addre) -> {
            addre.setUser(user);
        });
        Set<UserAddress> address = new HashSet<>(entities);
        try {
            user.setAddress(address);
            userFacade.edit(user);
            response.type = "success";
            response.message = "اطلاعات شما ذخیره شد.";
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.type = "error";
            response.message = "مشکل در ویرایش اطلاعات لطفا دوباره تلاش کنید";
            return response;
        }
    }

//    private DocumentsFacade
    public static String getExtension(String filename) {
        int p = filename.lastIndexOf(".");
        String e = filename.substring(p + 1);
        if (p == -1 || !e.matches("\\w+")) {/* file has no extension */
        } else {
            return e;
        }
        return "";
    }
}
