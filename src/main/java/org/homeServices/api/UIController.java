/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2013-2014 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * http://glassfish.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */
package org.homeServices.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import org.facades.CtgServicesFacade;
import org.facades.DocumentsFacade;
import org.facades.ObligationFacade;
import org.facades.OrderFacade;
import org.facades.ServicesFacade;
import org.facades.TokensFacade;
import org.facades.UserFacade;
import org.glassfish.jersey.server.mvc.Viewable;
import org.helper.token.Tokens;
import org.homeServices.model.CtgServices;
import org.homeServices.model.Services;

/**
 * @author Pavel Bucek (pavel.bucek at oracle.com)
 */
@Path("/")
@Stateless
public class UIController {

    @Context
    SecurityContext securityContext;

    @EJB
    private UserFacade userFacade;
    @EJB
    private ServicesFacade servicesFacade;
    @EJB
    private OrderFacade orderFacade;
    @EJB
    private ObligationFacade obligationFacade;
    @EJB
    private DocumentsFacade documentsFacade;
    @EJB
    private CtgServicesFacade ctgServicesFacade;

    @GET
    @Path("hello")
    @Produces(MediaType.TEXT_HTML)
    public Viewable getHello() {
        final Map<String, Object> map = new HashMap<>();
        map.put("user", "Pavel");
        final List<String> list = new ArrayList<>();

        list.add("item3");

//        uf.findAll().forEach(s -> list.add(s.getFldname()));
//        servicesFacade.findAll().forEach(s-> list.add(s.getFldtitle()));
//        orderFacade.findAll().forEach(s->list.add(s.getFldarea()));
//        osf.findAll().forEach(s->list.add(s.getFldid().toString()));
//        of.findAll().forEach(s->list.add(s.getFlddesc()));
//        df.findAll().forEach(s->list.add(s.getFlddesc()));
//        csf.findAll().forEach(s->list.add(s.getFlddesc()));
        map.put("items", list);
//        User tc = new User();
//        tc.setName("mostafa");
//        tc.setFamily("test 123");
//        tc.setType(3);

        return new Viewable("/hello.ftl", map);
    }

    @GET
    @Path("get-services")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Services> getAllServices() {
//        SmsFunction.sendSimpleSms("09373298343", "Test Message");
        return servicesFacade.findAll();
    }

    @GET
    @Path("getCtgServices/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Services> getCtgService(@PathParam("id") Integer id) {
        CtgServices services = ctgServicesFacade.find(id);
        if (services != null) {
            return (List<Services>) services.getServices();
        }
        return new ArrayList<>();
    }

    @GET
    @Path(value = "categories")
    @Produces(value = {MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void getAllCtg(@Suspended final AsyncResponse asyncResponse) {
                asyncResponse.resume(doGetAllCtg());
            }

    private List<CtgServices> doGetAllCtg() {
        return ctgServicesFacade.findAll();
    }
    @EJB
    private TokensFacade tokenFacade;

    @GET
    @Path("/tokens")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tokens> getAllToken() {
        tokenFacade.writeToken("09373298343", 0, 0);
        return tokenFacade.findAll();
    }
    
    
}
