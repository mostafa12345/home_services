
package org.homeServices.model;

import java.io.Serializable;


public class Credentials implements Serializable {

    private String mobile;
    private String token;
    private Integer device;
    private Role roles;

    public Credentials() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getDevice() {
        return device;
    }

    public void setDevice(Integer device) {
        this.device = device;
    }
    
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    
    public Role getRoles() {
        return roles;
    }

    public void setRoles(Role roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Credentials{" + "mobile=" + mobile + ", token=" + token + ", device=" + device + ", roles=" + roles + '}';
    }
    
}
