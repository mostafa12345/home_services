package org.homeServices.model;
// Generated May 14, 2017 1:00:10 PM by Hibernate Tools 4.3.1

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_services",
        catalog = "home_service"
)
@Access(value = AccessType.PROPERTY)
public class Services implements java.io.Serializable {

    private Integer id;
    private CtgServices ctg;
    private Integer countComment;
    private Date createdAt;
    private Integer deleted = 0;
    private String desc;
    private String file;
    private String meta;
    private Integer offer;
    private String point;
    private String polygon;
    private Integer price;
    private Float rate;
    private String tags;
    private String title;
    private Date updatedAt;
    private boolean active;
    private Set<Question> questions = new HashSet<Question>(0);
    private Set<SubServices> subServices = new HashSet<SubServices>(0);
    private Set<Documents> documents = new HashSet<Documents>(0);
    private Set<Comment> comments = new HashSet<Comment>(0);
    private Set<Obligation> obligations = new HashSet<Obligation>(0);
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fld_services_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Services() {
    }

    public Services(CtgServices ctg, Integer countComment, Date createdAt, Integer deleted, String desc, String file, String meta, Integer offer, String point, String polygon, Integer price, Float rate, String tags, String title, Date updatedAt, Set<Question> questions, Set<SubServices> subServices, Set<Documents> documents, Set<Comment> comments, Set<Obligation> obligations) {
        this.ctg = ctg;
        this.countComment = countComment;
        this.deleted = deleted;
        this.desc = desc;
        this.file = file;
        this.meta = meta;
        this.offer = offer;
        this.point = point;
        this.polygon = polygon;
        this.price = price;
        this.rate = rate;
        this.tags = tags;
        this.title = title;
        this.questions = questions;
        this.subServices = subServices;
        this.documents = documents;
        this.comments = comments;
        this.obligations = obligations;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "fld_id", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fld_tblctg_id")
    public CtgServices getCtg() {
        return this.ctg;
    }

    public void setCtg(CtgServices ctg) {
        this.ctg = ctg;
    }

    @Column(name = "fld_count_comment")
    public Integer getCountComment() {
        return this.countComment;
    }

    public void setCountComment(Integer countComment) {
        this.countComment = countComment;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fld_created_at", length = 19)
    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "fld_deleted")
    public Integer getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    @Column(name = "fld_desc")
    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Column(name = "fld_file")
    public String getFile() {
        return this.file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Column(name = "fld_meta", length = 3000)
    public String getMeta() {
        return this.meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    @Column(name = "fld_offer")
    public Integer getOffer() {
        return this.offer;
    }

    public void setOffer(Integer offer) {
        this.offer = offer;
    }

    @Column(name = "fld_point")
    public String getPoint() {
        return this.point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    @Column(name = "fld_polygon")
    public String getPolygon() {
        return this.polygon;
    }

    public void setPolygon(String polygon) {
        this.polygon = polygon;
    }

    @Column(name = "fld_price")
    public Integer getPrice() {
        return this.price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Column(name = "fld_rate", precision = 12, scale = 0)
    public Float getRate() {
        return this.rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    @Column(name = "fld_tags", length = 300)
    public String getTags() {
        return this.tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @Column(name = "fld_title")
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fld_updated_at", length = 19)
    public Date getUpdatedAt() {
        return this.updatedAt;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "services")
    public Set<Question> getQuestions() {
        return this.questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "services")
    public Set<SubServices> getSubServices() {
        return this.subServices;
    }

    public void setSubServices(Set<SubServices> subServices) {
        this.subServices = subServices;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "services")
    public Set<Documents> getDocuments() {
        return this.documents;
    }

    public void setDocuments(Set<Documents> documents) {
        this.documents = documents;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "services")
    public Set<Comment> getComments() {
        return this.comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "services")
    public Set<Obligation> getObligations() {
        return this.obligations;
    }

    public void setObligations(Set<Obligation> obligations) {
        this.obligations = obligations;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    @PrePersist
    private void prePersist() {
        this.meta = tags + " " + title;
        this.createdAt = new Date();
    }

    @PreUpdate
    private void PreUpdate() {
        this.meta = tags + " " + title;
        this.updatedAt = new Date();
    }

    @Override
    public String toString() {
        return "Services{" + "id=" + id + ", ctg=" + ctg + ", countComment=" + countComment + ", createdAt=" + createdAt + ", deleted=" + deleted + ", desc=" + desc + ", file=" + file + ", meta=" + meta + ", offer=" + offer + ", point=" + point + ", polygon=" + polygon + ", price=" + price + ", rate=" + rate + ", tags=" + tags + ", title=" + title + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        Services other = (Services) object;
        if (this.getId() != other.getId() && (this.getId() == null || !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    
}
