package org.homeServices;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.homeServices.api.UIController;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.freemarker.FreemarkerMvcFeature;
import org.homeServices.api.AuthController;
import org.homeServices.api.PanelController;
import org.homeServices.filter.AuthenticationFilter;
import org.homeServices.filter.AuthorizationFilter;
import org.homeServices.filter.CORSFilter;
import org.services.api_facade.CtgServicesFacadeREST;
import org.services.api_facade.DocumentsFacadeREST;
import org.services.api_facade.FullInfoUserFacadeREST;
import org.services.api_facade.ObligationFacadeREST;
import org.services.api_facade.OrderFacadeREST;
import org.services.api_facade.QuestionFacadeREST;
import org.services.api_facade.RateFacadeREST;
import org.services.api_facade.ServicesFacadeREST;
import org.services.api_facade.ServicesProvidersFacadeREST;
import org.services.api_facade.UserAddressFacadeREST;
import org.services.api_facade.UserFacadeREST;

public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        super(UIController.class);
        register(AuthController.class);
        register(LoggingFeature.class);
        register(FreemarkerMvcFeature.class);
        register(AuthenticationFilter.class);
        register(AuthorizationFilter.class);
        register(CORSFilter.class);
        register(PanelController.class);
        register(new JacksonJsonProvider());
        register(CtgServicesFacadeREST.class);
        register(DocumentsFacadeREST.class);
        register(FullInfoUserFacadeREST.class);
        register(ObligationFacadeREST.class);
        register(OrderFacadeREST.class);
        register(QuestionFacadeREST.class);
        register(RateFacadeREST.class);
        register(ServicesFacadeREST.class);
        register(ServicesProvidersFacadeREST.class);
        register(UserAddressFacadeREST.class);
        register(UserFacadeREST.class);
        register(MultiPartFeature.class);
    }
}
